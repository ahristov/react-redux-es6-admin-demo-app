// Add WebPack to use the included CommonsChunkPlugin
let webpack = require('webpack');
let path = require('path');

let config = {
	addVendor: function(name, path, parse) {
		this.resolve.alias[name] = path;
		if (!parse) {
      // this.module.noParse.push(new RegExp(path));
      this.module.noParse.push(new RegExp('^' + name + '$'));
		}
	},

  // We split the entry into two specific chunks. Our app and vendors. Vendors
  // specify that react should be part of that chunk
  entry: {
    app: ['webpack/hot/dev-server', './src/index.js'],
    vendors: ['bootstrap.css']
  },

	resolve: { alias: {} },

  // We add a plugin called CommonsChunkPlugin that will take the vendors chunk
  // and create a vendors.js file. As you can see the first argument matches the key
  // of the entry, "vendors"
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
  ],

  output: {
    // If in production mode we put the files into the dist folder instead
    path: process.env.NODE_ENV === 'production' ? './dist' : './build',
    filename: 'components.js'
  },

	module: {
    noParse: [],
    preLoaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'source-map'
      }
    ], // preloaders
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loaders: [
          'react-hot',
          'babel?presets[]=stage-0,presets[]=react,presets[]=es2015'
        ]
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loaders: [
          'url?limit=8192',
          'img'
        ]
      },
			{test: /\.css$/, loader: "style-loader!css-loader" },
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      {test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
		] // loaders
	} // module
};

// config.addVendor('react', m + '/react/react.min.js');
// config.addVendor('mui', bower_dir + '/mui/packages/cdn/js/mui.min.js');

let node_modules_dir = __dirname + '/node_modules';

config.addVendor('bootstrap.css', node_modules_dir + '/bootstrap/dist/css/bootstrap.min.css', true);

module.exports = config;
