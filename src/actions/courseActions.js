export function createCourse(course) {
  return { type: 'CREATE_COURSE', course };

  // ES6: shorthand property name
  //
  // => if right side name matches the left side name we can ommit it.
  //
  // above is same as same as:
  //
  //    return { type: 'CREATE_COURSE', course : course };
  //

}
