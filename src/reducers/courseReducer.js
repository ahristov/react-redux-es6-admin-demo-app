export default function courceReducer(state = [], action) {
  switch(action.type) {
    case 'CREATE_COURSE':
      return [...state,
        Object.assign({}, action.course)
      ];

      // ES6: Spread operator
      //
      //    '...state': spread the array, return a new instance of the original `state`
      //    Object.assign({}, action.course): create a copy of the object `action.course`
      //
      // => this is used for immutability
      //
      // whereas this:
      //
      //    state.push(action.course);
      //    return state;
      //
      // is wrong. It modifies the parameter.
      //


    default:
      return state;
  }
}
