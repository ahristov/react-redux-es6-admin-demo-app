import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as courseActions from '../../actions/courseActions';
class CoursesPage extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      course: { title: '' }
    };

    this.onTitleChange = this.onTitleChange.bind(this);
    this.onSaveClick = this.onSaveClick.bind(this);
  }

  onTitleChange(event) {
    const course = this.state.course;
    course.title = event.target.value;
    this.setState({ course : course });
  }

  onSaveClick() {
    this.props.dispatch(courseActions.createCourse(this.state.course));
  }

  courseRow(course, index) {
    return <div key={index}>{course.title}</div>;
  }

  render() {
    return (
      <div>
        <h1>Courses</h1>

        {this.props.courses.map(this.courseRow)}

        <h2>Add Course</h2>

        <input
          type="text"
          onChange={this.onTitleChange}
          value={this.state.course.title} />

        <input
          type="submit"
          value="Save"
          onClick={this.onSaveClick} />

      </div>
    );
  }
}

CoursesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  courses: PropTypes.array.isRequired
};

// What properties we want exposed on our components.
function mapStateToProps(state, ownProps) {
  return {
    courses: state.courses
  };
}

// What actions we want exposed on your component
// It is a second and optional parameter to connect().
// If we skip it (like below), what happens is, to the component gets exposed:
//  this.props.dispatch
// which is a function that allows to fire the actions
function mapDispatchToProps() {}

export default connect(mapStateToProps)(CoursesPage);
