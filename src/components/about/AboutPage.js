import React, {Component} from 'react';

class AboutPage extends Component {
  render() {
    return (
      <div>
        <h1>About</h1>
        <p>Thid application uses React, Redux, react Router and other helpful libraries.</p>
      </div>
    );
  }
}

export default AboutPage;
