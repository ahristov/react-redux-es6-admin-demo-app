# Building Apps with React and Redux in ES6

This code is from my following of the Pluralsight course:
[Building Application with React and Redux in ES6](https://app.pluralsight.com/library/courses/react-redux-react-router-es6/table-of-contents)
from  [Cory House](http://app.pluralsight.com/author/cory-house)


The starter code for the course can be found at:
[Course Starter Code](http://github.com/coryhouse/pluralsight-redux-starter)

Note: In this course the author is not using his popular starter kit:
[React Slingshot](https://github.com/coryhouse/react-slingshot)


## Usage

To start the application run:

  npm install
  npm start -s










